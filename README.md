===== Requirements ====

 - libapr1

===== Configuration ====

<Connector port="8080" protocol="org.apache.coyote.http11.Http11NioProtocol"
        maxThreads="150" connectionTimeout="20000" redirectPort="8443"
/>

===== Deploy ====

@Tomcat 6.0.24

- Deploy web archive(war) in $CATALINE_HOME/webapps
- Start Tomcat ($CATALINE_HOME/startup.sh)
