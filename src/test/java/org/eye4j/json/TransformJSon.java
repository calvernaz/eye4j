package org.eye4j.json;

import java.util.ArrayList;
import java.util.List;

import org.eye4j.core.Arguments;
import org.eye4j.core.Request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

public class TransformJSon {

    public static void main(final String[] args1) {

        final Gson gson = new GsonBuilder().setPrettyPrinting().create();

        final Request request = new Request();
        request.setTag("tag-1");
        request.setMethod("default-method");
        final List<Arguments> args = new ArrayList<Arguments>();
        final Arguments arguments = new Arguments();
        arguments.setKey("key1");
        arguments.setValue("value1");
        args.add(arguments);
        request.setArguments(args);
        final String json2 = gson.toJson(request);

        try {
            final Request requestq = gson.fromJson(json2, Request.class);
            System.out.println(" -> " + requestq);

            for (final Object a : requestq.getArguments()) {
                System.out.println("key: " + ((Arguments) a).getKey() + " values: " + ((Arguments) a).getValue());
            }

        } catch (final JsonParseException e) {
            System.out.println("Exception throwned: " + e.getMessage());
        }

    }
}
