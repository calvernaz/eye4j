/**
 * 
 */
package org.eye4j.runtim;

import junit.framework.TestCase;

import org.eye4j.runtime.Configuration;

/**
 * @author kron
 * 
 */
public class ConfigurationTest extends TestCase {

    public void testGetServices() {

        final Configuration configuration = Configuration.getInstance();

        final String[] services = configuration.getServices();

        assertEquals(true, services.length == 2);

        assertEquals("MEDUSA", services[0]);
        assertEquals("OCTOPOS", services[1]);
    }

}
