/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import junit.framework.TestCase;

import org.apache.log4j.spi.LoggingEvent;
import org.eye4j.core.Viewer;
import org.eye4j.runtime.Runtime;

public class RuntimeTest extends TestCase {

    public RuntimeTest(final String name) {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        // TODO Auto-generated method stub
        super.setUp();
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        // TODO Auto-generated method stub
        super.tearDown();
    }

    public void testRuntimeSetup() throws InterruptedException, IOException {

        Runtime.getInstance();
        final BlockingQueue<LoggingEvent> eventQueue = new LinkedBlockingQueue<LoggingEvent>(100);
        final SocketEndpoint endpoint = new SocketEndpoint(31339, eventQueue);
        final EventHandler eventHandler = new EventHandler(eventQueue, null);

        endpoint.listen();

        eventHandler.addObserver(new Viewer(new OutputStream() {

            @Override
            public void write(final int b) throws IOException {
                final BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
                final char c = (char) b;
                System.out.println("ASCII OF:=" + c);
            }

        }));

        final Thread messageSenderThread = new Thread(eventHandler, "MessageSender[]");
        messageSenderThread.setDaemon(true);

        messageSenderThread.start();

    }
}
