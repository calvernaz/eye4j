/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import junit.framework.TestCase;

import org.apache.log4j.Category;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggingEvent;
import org.eye4j.core.Viewer;
import org.eye4j.database.Dao;

/**
 * Include here the purpose of the class
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">Cesar Alvernaz</a>
 * @see Include references to other classes or remove this line
 */
public class EventHandlerTest extends TestCase {

    /**
     * @param name
     */
    public EventHandlerTest(final String name) {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * This is a stupid test, just to test the producer-consumer problem and
     * test the Observer pattern.
     * 
     * @throws IOException
     */
    public void testHandlerSetup() throws IOException {
        final BlockingQueue<LoggingEvent> eventQueue = new LinkedBlockingQueue<LoggingEvent>(100);

        final EventHandler evt = new EventHandler(eventQueue, new Dao());
        evt.addObserver(new Viewer(new OutputStream() {

            @Override
            public void write(final int b) throws IOException {
                final BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
                final char c = (char) b;
                System.out.println("ASCII OF:=" + c);
            }

        }));

        final Thread messageSenderThread = new Thread(evt, "MessageSender[]");
        messageSenderThread.setDaemon(true);

        try {
            eventQueue.put(new LoggingEvent("category", Category.getRoot(), Priority.DEBUG, "Event Number 1", null));
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }

        messageSenderThread.start();

        try {
            eventQueue.put(new LoggingEvent("category", Category.getRoot(), Priority.DEBUG, "Event Number 2", null));
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }

        try {
            eventQueue.put(new LoggingEvent("category", Category.getRoot(), Priority.DEBUG, "Event Number 3", null));
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } finally {
            evt.cancel();
        }
    }
}
