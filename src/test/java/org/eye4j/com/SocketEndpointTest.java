/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.com;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import junit.framework.TestCase;

import org.apache.log4j.Category;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Include here the purpose of the class
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">Cesar Alvernaz</a>
 * @see Include references to other classes or remove this line
 */
public class SocketEndpointTest extends TestCase {

    /**
     * @param name
     */
    public SocketEndpointTest(final String name) {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testOpenConnection() {
        final BlockingQueue<LoggingEvent> eventQueue = new LinkedBlockingQueue<LoggingEvent>(100);
        final SocketEndpoint endpoint = new SocketEndpoint(31337, eventQueue);
        try {
            endpoint.listen();
        } catch (final IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }

        final Thread enpointThread = new Thread(endpoint, "Endpoint[]");
        enpointThread.setDaemon(true);

        Socket socket = null;
        try {
            socket = new Socket("localhost", 31337);
        } catch (final UnknownHostException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (final IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try {
            final ObjectOutputStream oo = new ObjectOutputStream(socket.getOutputStream());
            final LoggingEvent e1 = new LoggingEvent(this.getClass().getName(), Category.getRoot(), Priority.DEBUG,
                    "LoggingEvent row1", new Throwable());

            oo.writeObject(e1);

        } catch (final IOException e) {
            System.err.println("sender error: " + e.getMessage());
        }
    }

    public void testCloseConnection() {
    }
}
