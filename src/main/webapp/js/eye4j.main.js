var eye4j = {
	options: {
		autoscroll: true
	},
	gui: {
		// implemented in eye4j.gui.js
	},
	com: {
		// implemented in eye4j.com.js
	},
	service: [
	   {name: null, category: null, color: null, id: null}
	]
}

$(document).ready(function () {
	
	$('div.logwindow > div').live("click", function () {
	    var cacheBreakpointElem = $('div.logwindow');
	    if (cacheBreakpointElem.find('div.breakpoint-start').length > 0) {
	      if (cacheBreakpointElem.find('div.breakpoint-stop').length > 0) {
	        return;
	      }
	      $(this).addClass('breakpoint-stop');
	      $.each(eye4j.gui._getAllBetween('div.breakpoint-start', 'div.breakpoint-stop'), function (index, value) {
	         value.addClass('active');
	      });
	    } else {
	      $(this).addClass('breakpoint-start active');
	    }
	});
	
	eye4j.gui.toggleButton('a#connect-button', 'source', function (message, fn) {
	    $(this).html(message);
	    if (fn) {
	      fn.call(this, null);
	    }
	  });
	
	eye4j.gui.getGroups();
	
	$('#category .button').live('click', function () {
    var url = "/eye4j/cmd/category";
    jQuery.ajax({
          type: 'POST',
          url: url,
          dataType: 'json',
          data: JSON.stringify({
              method: "selectCategory",
              arguments:  [{
                "value": this.text
              }],
              tag: "tag-1"
          }),
          success: function(data) {
            console.log(data);
          },
          dataType: 'json'
    });
	});
});