$.extend(eye4j.gui, {
	_scrolldelay: {},
	_scrolldown: function() {
		if (eye4j.options.autoscroll)
			//$(".logwindow").attr({ scrollTop: $(".logwindow").attr("scrollHeight")});
			//$('div.logviewer').scrollTop = 10000;
			eye4j.gui._pageScroll();
	},
	_pageScroll: function() {
    	window.scrollBy(0,50); // horizontal and vertical scroll increments
    	//_scrolldelay = setTimeout('eye4j.gui._pageScroll()',100); // scrolls every 100 milliseconds
	},
	_getAllBetween: function (firstElement,lastElement) {
	    var firstElement = $(firstElement); // First Element
	    var lastElement = $(lastElement); // Last Element
	    var collection = new Array();
	    collection.push(firstElement); // Add First Element to Collection
	    $(firstElement).nextAll().each(function(){ // Traverse all siblings
	        var siblingClass  = $(this).attr("class"); // Get Sibling Class
	        if (siblingClass != 'breakpoint-stop') { // If Sib is not LastElement
	                collection.push($(this)); // Add Sibling to Collection
	        } else { // Else, if Sib is LastElement
					collection.push($(this));	        
	                return false; // Break Loop
	        }
	    });         
	    return collection; // Return Collection
	},
	addEntry: function(viewerid, logentry) {
		div = $('<div></div>').text(logentry);
		$('#'+viewerid).append(div);
		this._scrolldown();
	},
	toggleButton: function (selector, cssClass, callback) {
	    $(selector).toggle(function(){  
	      $(this).addClass(cssClass);
	      callback.apply(this, ["Close"]);
	    },
	    function(){
          $(this).removeClass(cssClass);
          callback.apply(this, ["Connect", eye4j.com.stopSocket]);
      });
	},
	addGroup: function (selector, html) {
	  $(selector).append(html);
	},
	getGroups: function () {
	  eye4j.com.getGroups();
	},
	clear: function (viewerid) {
		$('.logwindow').empty();
	},
	send: function () {
		/*
		var c = eye4j.gui._getAllBetween('div.breakpoint-start', 'div.breakpoint-stop');
		var lines = new Array();
		var body;
		$.each(c, function (index, value) {
			value.addClass('active');
			body = escape(value.html() + "\n");
    		lines.push(body);

		});
		
		window.open("mailto:cesar.alvernaz@gmail.com?attachment=" + lines);
		
	  var url = "http://pastebin.com/api/api_post.php";
    $.ajax({
        type: 'POST',
        url: url,
        data: "api_dev_key=e95ae8b67351661125ce0141bba05607&api_option=paste&api_paste_code=just some random text you",
        success: function(data){
            console.log(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus + " " + errorThrown);
        },
        dataType: 'script'
    }); */
	  
    var surl = "http://tinypaste.com/api/create.json";
    $.ajax({
        type: 'POST',
        xhrFields: {
          withCredentials: true
        },
        url: surl,
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        data: { paste: 'text' },
        dataType: "jsonp",
        success: function(msg) {
             console.log('msg: ' + msg);
        },
        error: function(xhr, status, error) { console.log('msg: ' + status + " " + error); },
        async: false,
        cache: false
    });
	}
});


