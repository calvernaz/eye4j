$.extend(eye4j.com, {
    /* xhr: {}, */
    _onreadystatechange: null,
    startComet: function(){
        
        var url = "http://127.0.0.1:8080/eye4j/openstream";
        
        $.stream(url, {
        			enableXDR: true,        
                    type: "http",
                    dataType: "json",
                    open: function(event, stream) {                      	

							switch (stream.readyState) {
								case 0:
								$('.inner p').html('Connecting...');
								$('#status').show().delay(1000).fadeOut();
								break;
								case 1:
								$('.inner p').html('Connected');
								$('#status').show().delay(1000).fadeOut();
                    			break;
						   }                       	
                    },
                    handleSend: function (event, options, stream) {
                    	//stream.send({message: "Hello"});
                    },
                    handleMessage: function (text, message) {
                    	var end = text.indexOf("\r\n", message.index);
        				if (end < 0) {
                			return false;
        				}
        
        				message.data = text.substring(message.index, end);
        				message.index = end + "\r\n".length;
                    
                    	eye4j.com._onSuccess(message.data);
                    	
                    },
                    error: function() {
                     	$('.inner p').html('Connection with errors');
                     	$('#status').delay(1000).hide();
                    },
                    close: function() {
          				$('.inner p').html('Close');
          				$('#status').show().delay(1000).fadeOut();
                    }
          });
        

          
        
        /*
        $.atmosphere.subscribe(url, eye4j.com._onSuccess, $.atmosphere.request = {
            contentType: "application/json",
            dataType: 'json',
            data : 'json',
            transport: 'long-polling',
            logLevel: 'debug'
        }); 
        
         var request =  new XMLHttpRequest();
         request.open("GET", url, true);
         request.setRequestHeader("Content-Type","application/x-javascript;charset=UTF-8");
         request.overrideMimeType("application/json");
         request.setRequestHeader('Accept', 'application/json');
         
         request.onreadystatechange = function() {
         	if (request.readyState == 4 || request.readyState == 2) {
            	if (request.status == 200 || request.status == 0){
                	if (request.responseText) {
                    	document.getElementById("logviewer_1").innerHTML = request.responseText;
                    }
                }
                eye4j.com.startComet();
            }
         };
                  
         request.send("");
         */
         /*
         	success: function (data, textStatus) {
         		console.log('success: ' + data);
         		eye4j.com._onSuccess(data);
         	},
         	error: eye4j.com._onError
         */
         
    	/*
        $.ajax({
         	url: url,
         	crossDomain: true,
         	cache: false,
         	async: true,
         	dataType: 'json',
         	jsonp: 'callback',
         	jsonpCallback: 'func',
         	success: function (data, textStatus) {
         		console.log('success: ' + data);
         		eye4j.com._onSuccess(data);
         	},
         	error: eye4j.com._onError
         });
         */
         
		 
         /*.done(function() { alert("success"); })
         .fail(function() { alert("error"); })
         .always(function() { alert("complete"); });; */
         /*
         $.getJSON(url + "?callback=?", 
			function(data) {
    			console.log (data);
    		});*/
 
 /*
 		$.jsonp({
   			 url: url +"?callback=?",
    		success: function(json) {
    			console.log(json);
    		},
    		error: function() {
    			console.log('error: ' + json);
    		}
		});
*/         
        
    },
    _onSuccess: function(response){
        if (response) {
        	var data = $.parseJSON(response);
        	var date = new Date(data.timeStamp);
        	eye4j.gui.addEntry('logviewer_1', date.toUTCString() + " " + data.threadName + " " + 
        						data.locationInfo.fullInfo + " - " + data.renderedMessage);
        }
        response.responseBody = null;
    },
    _onError: function(xhr, status, error){
        alert(xhr.responseText + ' 1 ' + status);
    },
    _onComplete: function(xhr, status){
        alert(xhr);
    },
    getGroups: function () {
    	var url = "/eye4j/cmd/groups";
    	jQuery.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: JSON.stringify({
                method: "getGroups",
                tag: "tag-1"
            }),
            success: function(data) {
            	
            	for (var s in data) {
            		var html = $('<a></a>').addClass("button").html(data[s].groupName);
                	eye4j.gui.addGroup("div#category", html);
            	}
            },
            dataType: 'json'
        });
    },
    conf: function(){
        var url = "/eye4j/cmd";
        $.ajax({
            type: 'POST',
            url: url,
            contenttype: 'application/json; charset=utf-8',
            data: JSON.stringify({
                method: "getDebugLevel",
                tag: "tag-1"
            }),
            success: function(data){
                eye4j.gui.addEntry('logviewer_1', "Configuration: " + data);
            },
            dataType: 'json'
        });
    },
    startSocket: function(){
        var url = "/cmd/start";
        $.ajax({
            type: 'POST',
            url: url,
            contenttype: 'application/json; charset=utf-8',
            data: JSON.stringify({
                method: "start",
                arguments: [{
                    "value": "50"
                }, {
                    "key": "12345"
                }],
                tag: "tag-1"
            }),
            success: function(data){
                eye4j.gui.addEntry('logviewer_1', "Errors starting receiving events: " + data.error);
            },
            dataType: 'json'
        });
        
        
    },
    stopSocket: function() {
    /*
        var url = "/cmd/stop";
        $.ajax({
            type: 'POST',
            url: url,
            contenttype: 'application/json; charset=utf-8',
            data: JSON.stringify({
                method: "stop",
                arguments: [{
                    "value": "50"
                }, {
                    "key": "12345"
                }],
                tag: "tag-1"
            }),
            success: function(data){
                eye4j.gui.addEntry('logviewer_1', "Errors stoping receiving events: " + data.error);
            },
            dataType: 'json'
        });
        */
        $.stream().close();
        
    }
    
});
