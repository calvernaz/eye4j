package org.eye4j.filter;

import org.eye4j.core.AbstractFilter;



/**
 * This class is the main implementation for the filter mechanism. It loads configuration on startup
 * and reloads on user demand.
 * 
 * @author calvernaz
 * @param <T>
 */
public class AndFilter<T> extends AbstractFilter<T> {
  
  private final AbstractFilter<T>[] filters;
  
  
  
  public AndFilter(final AbstractFilter<T>... filters) {
    this.filters = filters;
  }
  
  
  
  @Override
  public boolean passes(T object) {
    for (AbstractFilter<T> filter : filters) {
      if ( !filter.passes(object))
        return false; // short circuit
    }
    return true;
  }
  
  
  
  @Override
  public void dropFilter(String filter) {
    
  }
}