package org.eye4j.filter;

import java.util.HashSet;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.eye4j.core.AbstractFilter;
import org.eye4j.runtime.Configuration;



/**
 * This class acts as a container for filter, keep them as static singletons or as filter chain.
 * 
 * @author calvernaz
 */
public class FilterManager {
  
  private static final Logger logger = Logger.getLogger(FilterManager.class);
  
  /**
   * Filter event by hierarchy. Check the table for hierarchy information.
   * <url>https://supportweb.cs.bham.ac.uk/documentation
   * /tutorials/docsystem/build/tutorials/log4j/log4j.html</url>
   * 
   * @param event
   * the event
   * @return true, if the value configured if greater or equal.
   */
  public static final AbstractFilter<LoggingEvent> LEVEL = new AbstractFilter<LoggingEvent>() {
    
    @Override
    public boolean passes(LoggingEvent log) {
      
      if ( !isActive()) {
        return false;
      }
      
      final Level defaultLevel = Level.toLevel(Configuration.getInstance().getDefaultLevel());
      if (defaultLevel.getSyslogEquivalent() >= log.getLevel().getSyslogEquivalent()) {
        return true;
      }
      return false;
    }
    
    
    
    @Override
    public void dropFilter(String filter) {
      // TODO Auto-generated method stub
      
    }
    
  };
  
  public static final AbstractFilter<LoggingEvent> GROUPS = new AbstractFilter<LoggingEvent>() {
    
    private final HashSet<String> vals = new HashSet<String>();
    {
      for (String[] str : Configuration.getInstance().getGroupsCategory().values()) {
        for (String s : str) {
          vals.add(s);
        }
      }
    }
    
    
    
    @Override
    public boolean passes(LoggingEvent log) {
      
      if ( !isActive()) {
        return false;
      }
      
      final String loggerName = log.getLoggerName();
      for (String category : vals) {
        
        // Case 1 - Exact match
        if (loggerName.equals(category)) {
          return true;
        }
        
        // Case 2 - Valid prefix
        if (loggerName.startsWith(category)) {
          return true;
        }
      }
      
      return false;
    }
    
    
    
    @Override
    public void dropFilter(String filter) {
      
      Map<String, String[]> categories = Configuration.getInstance().getGroupsCategory();
      String[] cats = categories.get(filter);
      
      vals.clear();
      for (String s : cats) {
        vals.add(s);
      }
      /*
       * String[] cats = categories.get(filter);
       * ArrayList<String[]> temp = new ArrayList<String[]>() {
       * @Override
       * public boolean contains(Object o) {
       * String pck = (String)o;
       * for (String[] str : this) {
       * for (String s : str) {
       * if (s.equals(pck)) {
       * return true;
       * }
       * }
       * }
       * return false;
       * };
       * };
       * temp.add(cats);
       * if ( !vals.containsAll(temp)) {
       * for (String[] str : temp) {
       * for (String s : str) {
       * vals.add(s);
       * }
       * }
       * }
       * vals.retainAll(temp);
       */
      
    }
  };
  
  @SuppressWarnings("unchecked")
  public static final AndFilter<LoggingEvent> COMPOUND = new AndFilter<LoggingEvent>(LEVEL, GROUPS);
}
