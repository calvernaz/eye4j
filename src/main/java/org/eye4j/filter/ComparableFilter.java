package org.eye4j.filter;

import org.eye4j.core.AbstractFilter;



public abstract class ComparableFilter<T extends Comparable<T>>
    extends AbstractFilter<T> {
  
  private final T comparable;
  
  
  
  protected ComparableFilter(T comparable) {
    this.comparable = comparable;
  }
  
  
  
  @Override
  public boolean passes(T object) {
    return passes(object.compareTo(comparable));
  }
  
  
  
  protected abstract boolean passes(int result);
}
