/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.catalina.comet.CometEvent;
import org.apache.catalina.comet.CometProcessor;
import org.apache.log4j.Logger;
import org.eye4j.runtime.Runtime;

/**
 * The implementation of Comet interface.
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">name</a>
 */
public class LogViewerServiceImpl extends LogViewerService implements CometProcessor {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(LogViewerServiceImpl.class);

    /**
     * Describe what the <code>serialVersionUID</code> is
     */
    private static final long serialVersionUID = 1L;

    /**
     * A map of connections (HttpServletResponses) that are in progress by HTTP
     * session ID. These connections are added and removed depending on the
     * Comet events that are triggered.
     */
    protected ConcurrentMap<String, HttpServletResponse> connections = new ConcurrentHashMap<String, HttpServletResponse>();

    /**
     * A map of viewer by session ID. This method provided, because there is no
     * particularly easy way to remove viewers from observables (by reference)
     * So next to adding the observers to the observable in the first place, we
     * need to keep a reference to be able to remove it later.
     */
    // protected ConcurrentMap<String, Viewer> viewers = new
    // ConcurrentHashMap<String, Viewer>();

    @Override
    public void init() throws ServletException {
        super.init();
        logger.debug("LogViewerServiceImpl Servlet init");
    }

    /**
     * Once a connection is made by client, this method is called.
     */
    @Override
    protected void eventBegin(final HttpServletRequest request, final HttpServletResponse response) {

        final String sessionId = request.getSession().getId();
        logger.debug("eventBegin called for session: " + sessionId);

        // NOTE: really necessary ?
        response.setContentType("text/javascript");
        //response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        

        synchronized (this.connections) {

            // only register one connection per id.
            if (!this.connections.containsKey(sessionId)) {
                this.connections.put(sessionId, response);

                try {
                    Runtime.getInstance()
                            .registerObserver(sessionId, this.connections.get(sessionId).getOutputStream());
                } catch (final IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    protected void eventEnd(final HttpServletRequest request, final HttpServletResponse response) {

        final String sessionId = request.getSession().getId();
        logger.debug("eventEnd called for session: " + sessionId);
        synchronized (this.connections) {

            try {
                this.connections.get(sessionId).getOutputStream().close();
            } catch (final IOException e) {
                e.printStackTrace();
            }
            this.connections.remove(sessionId);
            // Runtime.getInstance().stopEndpoint();
            Runtime.getInstance().unregisterObserver(sessionId);

        }

    }

    @Override
    protected void eventError(final HttpServletRequest request, final HttpServletResponse response) {

        final String sessionId = request.getSession().getId();
        logger.debug("eventError called for session: " + sessionId);
        synchronized (this.connections) {
            try {
                this.connections.get(sessionId).getOutputStream().close();
            } catch (final IOException e) {
                e.printStackTrace();
            }
            this.connections.remove(sessionId);
            // Runtime.getInstance().stopEndpoint();
            Runtime.getInstance().unregisterObserver(sessionId);

        }
    }

    @Override
    protected void eventRead(final CometEvent event) throws IOException {
        // need think about this !!
        final InputStream is = event.getHttpServletRequest().getInputStream();
        final byte[] buf = new byte[512];
        do {
            final int n = is.read(buf); // can throw an IOException
            if (n > 0) {
                logger.debug("Read " + n + " bytes: " + new String(buf, 0, n));
            } else if (n < 0) {
                event.close();
                return;
            }
        } while (is.available() > 0);
    }

}
