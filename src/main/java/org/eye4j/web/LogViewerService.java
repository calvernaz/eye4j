/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.catalina.comet.CometEvent;
import org.apache.log4j.Logger;

/**
 * Include here the purpose of the class
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">name</a>
 * @see Include references to other classes or remove this line
 */
public abstract class LogViewerService extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LogViewerService.class);

    /**
     * Describe what the <code>serialVersionUID</code> is
     */
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     * 
     * @see
     * 
     * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
     * , javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException,
            IOException {
        resp.setContentType("text/html");
        final PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Hello</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Configure to use Comet interface!!</h1>");
        out.println("</body>");
        out.println("</html>");

    }

    /**
     * Event.
     * 
     * @param event
     *            the event
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws UnsupportedOperationException
     *             the unsupported operation exception
     * @throws ServletException
     *             the servlet exception
     */
    public final void event(final CometEvent event) throws IOException, UnsupportedOperationException, ServletException {

        final HttpServletRequest request = event.getHttpServletRequest();
        final HttpServletResponse response = event.getHttpServletResponse();

        if (event.getEventType() == CometEvent.EventType.BEGIN) {
            /*
             * The comet session begins, disconnects after 30 seconds idle
             * without data
             */
            /* event.setTimeout(30 * 1000); */
            eventBegin(request, response);
        } else if (event.getEventType() == CometEvent.EventType.ERROR) {
            /*
             * The comet session errorred and from here onwards, the
             * request/response is invalid.
             */
            eventError(request, response);
            event.close();

        } else if (event.getEventType() == CometEvent.EventType.END) {
            /*
             * The comet session ends and from here onwards, the
             * request/response is invalid.
             */
            eventEnd(request, response);
            event.close();
        } else if (event.getEventType() == CometEvent.EventType.READ) {
            /*
             * Data available from the client. This framework doesn't use this.
             * I'm not feeling happy about using duplex client/server
             * communications without a proper protocol
             */
            eventRead(event);
        }
    }

    /**
     * protected events that are meant to be implemented by deriving classes.
     * 
     * @param request
     *            http servlet request
     * @param response
     *            http servlet response
     */
    protected abstract void eventBegin(HttpServletRequest request, HttpServletResponse response);

    protected abstract void eventError(HttpServletRequest request, HttpServletResponse response);

    protected abstract void eventRead(CometEvent event) throws IOException;

    protected abstract void eventEnd(HttpServletRequest request, HttpServletResponse response);
}
