/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.web;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eye4j.commands.GetGroupsCommand;
import org.eye4j.commands.SelectCategoryCommand;
import org.eye4j.commands.SelectLogsCommand;
import org.eye4j.commands.ServersListCommand;
import org.eye4j.commands.ServicesListCommand;
import org.eye4j.commands.StartCommand;
import org.eye4j.commands.StopCommand;
import org.eye4j.core.Request;
import org.eye4j.runtime.Configuration;
import org.eye4j.runtime.Runtime;



/**
 * This abstract class acts as support for the well-know pattern Command. All
 * concrete commands must extends this class, and implement the {@link #process()} method.
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">Cesar Alvernaz</a>
 */
public abstract class Command {
  
  private static final Runtime runtime = Runtime.getInstance();
  
  protected static final Configuration configuration = Configuration.getInstance();
  
  /** The context. */
  protected ServletContext context;
  
  /** The request. */
  protected HttpServletRequest request;
  
  /** The response. */
  protected HttpServletResponse response;
  
  
  
  /**
   * Inits the.
   * 
   * @param context
   * the context
   * @param request
   * the request
   * @param response
   * the response
   */
  public void init(final ServletContext context, final HttpServletRequest request, final HttpServletResponse response) {
    this.context = context;
    this.request = request;
    this.response = response;
  }
  
  
  
  /**
   * Process.
   * 
   * @param request
   * @throws ServletException
   * the servlet exception
   * @throws IOException
   * Signals that an I/O exception has occurred.
   */
  public abstract void process(Request request);
  
  
  
  /**
   * Forward.
   * 
   * @param target
   * the target
   * @throws ServletException
   * the servlet exception
   * @throws IOException
   * Signals that an I/O exception has occurred.
   */
  protected void forward(final String target) throws ServletException, IOException {
    final RequestDispatcher dispatcher = this.context.getRequestDispatcher(target);
    dispatcher.forward(this.request, this.response);
  }
  
  /**
   * This Map contains all commands and the matching command implementation
   * with specific behavior. Each command implement the method {@link Command#process()}
   */
  @SuppressWarnings("nls")
  protected static final Map<String, Class<? extends Command>> commands =
      new ConcurrentHashMap<String, Class<? extends Command>>() {
        
        /**
         * Describe what the <code>serialVersionUID</code> is
         */
        private static final long serialVersionUID = 1L;
        
        {
          put("getServersList", ServersListCommand.class);
          put("getServicesList", ServicesListCommand.class);
          put("selectLogs", SelectLogsCommand.class);
          put("/category", SelectCategoryCommand.class);
          put("/groups", GetGroupsCommand.class);
          put("/start", StartCommand.class);
          put("/stop", StopCommand.class);
        }
      };
  
  
  
  /**
   * Gets the runtime.
   * 
   * @return the runtime
   */
  protected Runtime getRuntime() {
    return Runtime.getInstance();
  }
  
  
  
  /**
   * Gets the configuration instance.
   * 
   * @return the configuration
   */
  protected Configuration getConfiguration() {
    return Configuration.getInstance();
  }
  
}
