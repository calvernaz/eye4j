/*
 * Copyright (C) The reproduction, transmission or use of this document or its
 * contents is not permitted without express written authorization. All rights,
 * including rights created by patent grant or registration of a utility model
 * or design, are reserved. Modifications made to this document are restricted
 * to authorized personnel only. Technical specifications and features are
 * binding only when specifically and expressly agreed upon in a written
 * contract.
 */
package org.eye4j.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eye4j.commands.UnknownCommand;
import org.eye4j.core.Request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;



/**
 * This class implements the behavior upon reception of a web request.
 * <p>
 * When a new POST arrives it validates the incoming data as a JSON string and dispatches the
 * request to the correspondent method.
 * <p>
 * TODO: describe the JSON method message
 */
@WebServlet("/cmd")
public class FrontController extends HttpServlet {
  
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8064549101792850382L;
  
  private static final Logger logger = Logger.getLogger(FrontController.class);
  
  
  
  @Override
  public void init() throws ServletException {
    super.init();
    logger.debug("FrontController Servlet init");
  }
  
  
  
  @Override
  public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException,
      IOException {
    super.doPost(req, resp);
  }
  
  
  
  /*
   * (non-Javadoc)
   * @see
   * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
   * , javax.servlet.http.HttpServletResponse)
   */
  @Override
  public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException,
      IOException {
    
    resp.addHeader("Access-Control-Allow-Origin", "*");
    resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
    // retrieve message from post
    final String requestMessage = getBody(req);
    
    // retrieve request object from JSON message
    final Request request = getRequest(requestMessage);
    
    Command command = null;
    if (request.getMethod() != null) {
      
      try {
        command = getCommand(req.getPathInfo());
        
        if (command != null) {
          command.init(getServletContext(), req, resp);
          command.process(request);
        }
        
      }
      catch (final InstantiationException e) {
        logger.error("Exception throwned: " + e.getMessage());
        e.printStackTrace();
      }
      catch (final IllegalAccessException e) {
        logger.error("Exception throwned: " + e.getMessage());
        e.printStackTrace();
      }
    }
  }
  
  
  
  /**
   * Gets the command.
   * 
   * @param command
   * the command
   * @param req
   * the req
   * @param resp
   * the resp
   * @return the command
   * @throws InstantiationException
   * the instantiation exception
   * @throws IllegalAccessException
   * the illegal access exception
   */
  private Command getCommand(final String command) throws InstantiationException, IllegalAccessException {
    if (logger.isDebugEnabled()) {
      logger.debug("Command to process: " + command);
    }
    return getCommandClass(command.substring(0,
      command.indexOf('/', 1) == -1 ? command.length() : command.indexOf('/', 1)));
  }
  
  
  
  /**
   * Gets the command class.
   * 
   * @param req
   * the req
   * @return the command class
   */
  @SuppressWarnings("unchecked")
  private static <T extends Command> T getCommandClass(final String command) {
    
    final Class<? extends Command> clazz = Command.commands.get(command);
    T commandClass = null;
    if (clazz != null) {
      
      try {
        commandClass = (T)clazz.newInstance();
      }
      catch (final IllegalArgumentException e) {
        e.printStackTrace();
      }
      catch (final InstantiationException e) {
        e.printStackTrace();
      }
      catch (final IllegalAccessException e) {
        e.printStackTrace();
      }
      catch (final SecurityException e) {
        e.printStackTrace();
      }
    }
    else {
      commandClass = (T)new UnknownCommand();
    }
    return commandClass;
  }
  
  
  
  /**
   * This method retrieves the content of body, for the POST method.
   * 
   * @param req
   * @return a json string
   * @throws IOException
   */
  private String getBody(final HttpServletRequest req) throws IOException {
    
    final StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      
      final BufferedReader reader = req.getReader();
      while ((line = reader.readLine()) != null) {
        jb.append(line);
      }
      
    }
    catch (final IOException e) {
      
      logger.error("Exception throwned: " + e.getMessage());
      throw e;
    }
    
    return jb.toString();
  }
  
  
  
  /**
   * Retrieves the internal representation of a {@link Request}.
   * 
   * @param jsonRequest
   * must be a JSON string representation.
   * @return a internal representation of a {@link Request}.
   */
  private Request getRequest(final String jsonRequest) {
    if (logger.isDebugEnabled()) {
      logger.debug("getRequest: " + jsonRequest);
    }
    
    final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    Request request = null;
    try {
      
      // request = gson.fromJson(jsonRequest, Request.class);
      final Type listType = new TypeToken<Request>() {
      }.getType();
      request = gson.fromJson(jsonRequest, listType);
      
    }
    catch (final JsonParseException e) {
      logger.error(e.getMessage());
      e.printStackTrace();
    }
    
    return request;
  }
}
