/**
 * 
 */
package org.eye4j.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.comet.CometEvent;
import org.apache.catalina.comet.CometProcessor;



/**
 * @author calvernaz
 */
public abstract class CometFrontend extends HttpServlet implements CometProcessor {
  
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  
  
  
  /**
	 * 
	 */
  public CometFrontend() {
    // TODO Auto-generated constructor stub
  }
  
  
  
  /*
   * (non-Javadoc)
   * @see org.apache.catalina.CometProcessor#event(org.apache.catalina.CometEvent)
   */
  @Override
  public void event(CometEvent event) throws IOException, ServletException {
    final HttpServletRequest request = event.getHttpServletRequest();
    final HttpServletResponse response = event.getHttpServletResponse();
    
    response.addHeader("Access-Control-Allow-Origin", "*");
    response.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
    
    if (event.getEventType() == CometEvent.EventType.BEGIN) {
      /*
       * The comet session begins, disconnects after 30 seconds idle
       * without data
       */
      event.setTimeout(30 * 1000);
      eventBegin(request, response);
    }
    else if (event.getEventType() == CometEvent.EventType.ERROR) {
      /*
       * The comet session errorred and from here onwards, the
       * request/response is invalid.
       */
      eventError(request, response);
      event.close();
      
    }
    else if (event.getEventType() == CometEvent.EventType.END) {
      /*
       * The comet session ends and from here onwards, the
       * request/response is invalid.
       */
      eventEnd(request, response);
      event.close();
    }
    else if (event.getEventType() == CometEvent.EventType.READ) {
      /*
       * Data available from the client. This framework doesn't use this.
       * I'm not feeling happy about using duplex client/server
       * communications without a proper protocol
       */
      eventRead(event);
    }
    
  }
  
  
  
  /**
   * protected events that are meant to be implemented by deriving classes.
   * 
   * @param request
   * http servlet request
   * @param response
   * http servlet response
   */
  protected abstract void eventBegin(HttpServletRequest request, HttpServletResponse response);
  
  
  
  protected abstract void eventError(HttpServletRequest request, HttpServletResponse response);
  
  
  
  protected abstract void eventRead(CometEvent event) throws IOException;
  
  
  
  protected abstract void eventEnd(HttpServletRequest request, HttpServletResponse response);
}
