/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eye4j.core.Category;
import org.eye4j.core.Request;
import org.eye4j.core.Response;
import org.eye4j.core.Service;
import org.eye4j.web.Command;

import com.google.gson.Gson;

/**
 * Include here the purpose of the class
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">name</a>
 * @see Include references to other classes or remove this line
 */
public class ServicesListCommand extends Command {

    private static final Logger logger = Logger.getLogger(ServicesListCommand.class);

    @Override
    public void process(final Request request) {
        logger.debug("ServicesListCommand process()");

        final Response resp = new Response();
        final Gson gson = new Gson();

        // resp.setResult("success");

        final Category category = new Category(1, "category");

        final Service service = new Service("service_1");

        final List<Service> services = new ArrayList<Service>();
        services.add(service);

        category.setServices(services);

        // resp.setArguments(category.getServices());

        // resp.setTag("");
        final String json = gson.toJson(response, Response.class);

        // final PrintWriter out = response.getWriter();
        // out.println(json);

    }

}
