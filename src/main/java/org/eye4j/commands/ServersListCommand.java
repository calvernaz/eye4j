package org.eye4j.commands;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eye4j.core.Arguments;
import org.eye4j.core.Category;
import org.eye4j.core.Request;
import org.eye4j.core.Response;
import org.eye4j.core.Service;
import org.eye4j.web.Command;

import com.google.gson.Gson;

public class ServersListCommand extends Command {

    private static final Logger logger = Logger.getLogger(ServicesListCommand.class);

    @Override
    public void process(final Request request) {
        logger.debug("ServersListCommand process()");

        // access to mongo-db ? maybe...
        final List<Arguments> args = request.getArguments();

        final Response resp = new Response();
        final Gson gson = new Gson();

        // resp.setResult("success");

        final Category category = new Category(1, "category");

        final Service service = new Service("service_1");

        final List<Service> services = new ArrayList<Service>();
        services.add(service);

        category.setServices(services);

        // resp.setArguments(category.getServices());

        // resp.setTag("");
        final String json = gson.toJson(resp, Response.class);

        response.setContentType("application/json");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter out;
        try {
            out = response.getWriter();
            out.println(json);
            out.close();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
