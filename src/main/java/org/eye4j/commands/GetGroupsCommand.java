/**
 * 
 */
package org.eye4j.commands;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eye4j.core.Group;
import org.eye4j.core.Request;
import org.eye4j.runtime.Configuration;
import org.eye4j.web.Command;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;



/**
 * @author calvernaz
 */
public class GetGroupsCommand extends Command {
  
  private static final Logger logger = Logger.getLogger(GetGroupsCommand.class);
  
  
  
  @Override
  public void process(Request request) {
    final Gson gson = new Gson();
    
    List<Group> listGroups = new ArrayList<Group>();
    Group group = null;
    Map<String, String[]> groupsCategory = Configuration.getInstance().getGroupsCategory();
    for (String s : groupsCategory.keySet()) {
      group = new Group(s, Arrays.asList(groupsCategory.get(s)));
      logger.debug("Group: " + s + " packages: " + Arrays.toString(groupsCategory.get(s)));
      listGroups.add(group);
    }
    
    OutputStreamWriter writer;
    response.setContentType("application/json");
    response.setHeader("Cache-Control", "no-cache");
    
    try {
      writer = new OutputStreamWriter(this.response.getOutputStream());
      Type typeOfMap = new TypeToken<List<Group>>() {
      }.getType();
      writer.write(gson.toJson(listGroups, typeOfMap));
      writer.flush();
      writer.close();
      
    }
    catch (final IOException e) {
      logger.error("Exception throwned: " + e.getMessage());
      e.printStackTrace();
    }
  }
  
}
