/**
 * 
 */
package org.eye4j.commands;

import java.util.List;

import org.apache.log4j.Logger;
import org.eye4j.core.Arguments;
import org.eye4j.core.Request;
import org.eye4j.filter.FilterManager;
import org.eye4j.web.Command;

import com.google.gson.Gson;



/**
 * @author calvernaz
 * @param <E>
 */
public class SelectCategoryCommand<E> extends Command {
  
  private static final Logger logger = Logger.getLogger(SelectCategoryCommand.class);
  
  
  
  @Override
  public void process(Request request) {
    final Gson gson = new Gson();
    
    List<Arguments> args = request.getArguments();
    
    String cat = (String)args.get(0).getValue();
    
    FilterManager.GROUPS.dropFilter(cat);
  }
}
