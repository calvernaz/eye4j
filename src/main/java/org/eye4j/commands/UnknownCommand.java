/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.commands;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.eye4j.core.Request;
import org.eye4j.core.Response;
import org.eye4j.web.Command;



/**
 * This is the classical unknown command error. It just print a message warning.
 * 
 * @author Cesar Alvernaz
 */
public class UnknownCommand extends Command {
  
  /** The Constant logger. */
  private static final Logger logger = Logger.getLogger(UnknownCommand.class);
  
  

  @Override
  public void process(final Request request) {
    
    PrintWriter writer;
    try {
      
      // get the servlet response
      writer = this.response.getWriter();
      
      // create a response
      final Response response = new Response();
      response.setError("true");
      response.setErrorMessage("unknow command: must be implemented");
      
      // send it
      writer.write(response.toString());
      writer.flush();
      
    }
    catch (final IOException e) {
      logger.error("Exception throwned: " + e.getMessage());
    }
  }
  
}
