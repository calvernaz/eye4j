/**
 * 
 */
package org.eye4j.commands;

import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;
import org.eye4j.core.Request;
import org.eye4j.core.Response;
import org.eye4j.web.Command;

/**
 * @author kron
 * 
 */
public class StartCommand extends Command {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(StartCommand.class);

    /**
     * 
     */
    public StartCommand() {
        // TODO Auto-generated constructor stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eye4j.web.Command#process(org.eye4j.core.Request)
     */
    @Override
    public void process(final Request request) {

        final Response response = new Response();

        getRuntime().startEndpoint();
        response.setError("false");

        OutputStreamWriter writer;
        try {
            writer = new OutputStreamWriter(this.response.getOutputStream());
            writer.write(response.toString());
            writer.flush();
            writer.close();

        } catch (final IOException e) {
            logger.error("Exception throwned: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
