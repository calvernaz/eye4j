/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.runtime;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;



/**
 * This class constaint all the configuration of eye4j.
 * <P>
 * This class is singleton.
 * 
 * @author Cesar Alvernaz, Mar 28, 2010
 */
public final class Configuration {
  
  private static final Logger logger = Logger.getLogger(Configuration.class);
  
  /**
   * Instance holder allows unsynchronized lazy initialization of the
   * singleton instance by the class loader.
   */
  private static final class InstanceHolder {
    
    /**
     * Singleton reference.
     */
    private final static Configuration INSTANCE = new Configuration();
    
    /*
     * Make some noise for user information
     */
    static {
      logger.debug("Default log level: " + Configuration.getInstance().getDefaultLevel());
      logger.debug("Services running: " + Arrays.toString(Configuration.getInstance().getServices()));
      logger.debug("Ports bounded: " + Arrays.toString(Configuration.getInstance().getPorts()));
      logger.debug("Groups configuration: " + Arrays.toString(Configuration.getInstance().getGroups()));
    }
    
    
    
    /**
     * Prevent instantiation.
     */
    private InstanceHolder() {
      // Empty
    }
  }
  
  /**
   * Reference for the properties.
   */
  private final Properties properties;
  
  
  
  /**
   * Constructor.
   */
  protected Configuration() {
    this.properties = new Properties();
    
    try {
      final InputStream in = Configuration.class.getResourceAsStream("/eye4j.properties");
      if (in == null) {
        logger.warn("Using default configuration. Cause: configuration file not found");
        return;
      }
      
      this.properties.load(in);
      in.close();
    }
    catch (final IOException e) {
      logger.warn("Using default configuration. Cause: " + e.getMessage());
    }
  }
  
  
  
  /**
   * Get the singleton reference.
   * 
   * @return the singleton reference
   */
  public static Configuration getInstance() {
    return InstanceHolder.INSTANCE;
  }
  
  
  
  /**
   * Get the class name for the application runtime.
   * <P>
   * First, it will check the environment property <code>eye4j.application.runtime</code> and then
   * the configuration file.
   * 
   * @return the class name for the application runtime
   */
  public String getApplicationRuntimeName() {
    final String applicationRuntime = System.getProperty("eye4j.application.runtime");
    if ((applicationRuntime != null) && (applicationRuntime.length() > 0)) {
      return applicationRuntime;
    }
    
    return this.properties.getProperty("APPLICATION_RUNTIME");
  }
  
  
  
  /**
   * Gets the service port.
   * 
   * @param serviceName
   * the service name
   * @return the service port
   */
  public String getServicePort(final String serviceName) {
    if (logger.isDebugEnabled()) {
      logger.debug("getServicePort ( " + serviceName + " )");
    }
    return this.properties.getProperty(serviceName);
  }
  
  
  
  /**
   * Gets the services.
   * 
   * @return a array of strings that need to be 'trimmed'
   */
  public String[] getServices() {
    final String services = properties.getProperty("SERVICES");
    
    if (services != null) {
      final String[] srv = services.split(",\\s+");
      return srv;
    }
    return new String[0];
  }
  
  
  
  /**
   * Gets the ports.
   * 
   * @return the ports
   */
  public String[] getPorts() {
    
    int i = 0;
    final String[] ports = new String[getServices().length];
    for (final String service : getServices()) {
      final String port = (String)this.properties.get(service);
      ports[i++] = port;
    }
    
    return ports;
  }
  
  
  
  public String[] getGroups() {
    final String groups = properties.getProperty("GROUPS");
    
    if (groups != null) {
      final String[] srv = groups.split(",\\s+");
      return srv;
    }
    return new String[0];
  }
  
  
  
  public Map<String, String[]> getGroupsCategory() {
    
    final HashMap<String, String[]> groupsCategory = new HashMap<String, String[]>();
    
    for (final String groupCategory : getGroups()) {
      String cats = (String)this.properties.get(groupCategory);
      final String[] srv = cats.split(",\\s+");
      groupsCategory.put(groupCategory, srv);
    }
    
    return groupsCategory;
  }
  
  
  
  /**
   * Gets the default level of logging.
   * 
   * @return the default level
   */
  public String getDefaultLevel() {
    return this.properties.getProperty("DEFAULT_LEVEL");
  }
  
  
  
  /**
   * Sets the default level.
   * 
   * @param level
   * the new default leve
   */
  public void setDefaultLevel(final String level) {
    this.properties.setProperty("DEFAULT_LEVEL", level);
  }
  
}