/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.SocketHandler;

import javax.servlet.ServletOutputStream;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.eye4j.com.Endpoint;
import org.eye4j.com.EventHandler;
import org.eye4j.com.SocketEndpoint;
import org.eye4j.core.Viewer;
import org.eye4j.database.Dao;



/**
 * Include here the purpose of the class.
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">name</a>
 * @see Include references to other classes or remove this line
 */
public final class Runtime {
  
  /** The Constant logger. */
  private static final Logger logger = Logger.getLogger(Runtime.class);
  
  /** The runtime instance. */
  private static final Runtime runtime = new Runtime();
  
  private final Map<String, Viewer> observers = new ConcurrentHashMap<String, Viewer>();
  
  /**
   * The event queue is the shared resource, between {@link SocketHandler} and. {@link EventHandler}
   */
  private final BlockingQueue<LoggingEvent> eventQueue;
  
  private ArrayList<SocketEndpoint> endpoints = null;
  
  /** The endpoint. */
  private final SocketEndpoint endpoint = null;
  
  /** The event handler. */
  private final EventHandler eventHandler;
  
  
  
  /**
   * Instantiates a new runtime.
   * 
   * @throws IOException
   */
  @SuppressWarnings("boxing")
  private Runtime() {
    
    // shared queue
    this.eventQueue = new LinkedBlockingQueue<LoggingEvent>(100);
    
    this.endpoints = new ArrayList<SocketEndpoint>(Configuration.getInstance().getServices().length);
    
    int i = 0;
    for (final String services : Configuration.getInstance().getServices()) {
      this.endpoints.add(new SocketEndpoint(Integer.valueOf(Configuration.getInstance().getPorts()[i]),
                                            this.eventQueue));
      ++i;
    }
    
    logger.debug("Before StartEndpoint");
    // setup the endpoint
    // this.endpoint = new SocketEndpoint(Service.DEFAULT_PORT,
    // this.eventQueue);
    
    startEndpoint();
    
    // setup eventhandler
    this.eventHandler = new EventHandler(this.eventQueue, new Dao());
    final Thread eventHandlerThread = new Thread(this.eventHandler, "EventHandler-Thread");
    
    // start eventhandler
    eventHandlerThread.setDaemon(true);
    eventHandlerThread.start();
    
  }
  
  
  
  /**
   * Gets the single instance of Runtime.
   * 
   * @return single instance of Runtime
   */
  public static Runtime getInstance() {
    return runtime;
  }
  
  
  
  /**
   * Start endpoint socket connection.
   * 
   * @throws IOException
   * Signals that an I/O exception has occurred.
   */
  public void startEndpoint() {
    logger.debug("startEndpoint ()");
    try {
      for (final Endpoint endpoint : endpoints) {
        endpoint.listen();
      }
    }
    catch (final IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  
  
  /**
   * Stop endpoint socket connection.
   * 
   * @throws IOException
   */
  public void stopEndpoint() throws IOException {
    logger.debug("stopEndpoint ()");
    this.endpoint.disconnect();
  }
  
  
  
  /**
   * TODO: viewer must be taken off as 'observer' Cancel viewer.
   * 
   * @param sessionId
   * TODO
   */
  public void unregisterObserver(final String sessionId) {
    if (logger.isDebugEnabled()) {
      logger.debug("unregister observer: " + sessionId);
    }
    
    // delete observer list, keep it clean
    this.eventHandler.deleteObserver(this.observers.get(sessionId));
  }
  
  
  
  /**
   * Register observer.
   * 
   * @param sessionId
   * the session id
   * @param servletOutputStream
   * the servlet output stream
   * @throws IOException
   * Signals that an I/O exception has occurred.
   */
  public void registerObserver(final String sessionId, final ServletOutputStream servletOutputStream)
      throws IOException {
    if (logger.isDebugEnabled()) {
      logger.debug("registerObserver: " + sessionId);
    }
    
    addViewer(sessionId, new Viewer(servletOutputStream));
  }
  
  
  
  /**
   * Adds the viewer.
   * 
   * @param sessionId
   * TODO
   * @param viewer
   * the viewer
   */
  private void addViewer(final String sessionId, final Observer viewer) {
    
    /* add the observer to a map, for future delete. */
    this.observers.put(sessionId, (Viewer)viewer);
    
    /* viewer will be notified by events consumed in this eventHandler */
    this.eventHandler.addObserver(viewer);
  }
  
}
