/*
 * Copyright (C) The reproduction, transmission or use of this document or its
 * contents is not permitted without express written authorization. All rights,
 * including rights created by patent grant or registration of a utility model
 * or design, are reserved. Modifications made to this document are restricted
 * to authorized personnel only. Technical specifications and features are
 * binding only when specifically and expressly agreed upon in a written
 * contract.
 */
package org.eye4j.com;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

/**
 * This class represents an {@link Endpoint} for communication over socket
 * protocol. It provides a <code>ServerSocket</code> with that clients can
 * connect, and starts for each incoming connection a {@link SocketHandler} that
 * handles {@link LoggingEvent}s from peers.
 * 
 * @author Cesar Alvernaz, Jan 10, 2010
 */
public final class SocketEndpoint extends Endpoint implements Runnable {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(SocketEndpoint.class);

    /**
     * {@link Set} containing all {@link SocketHandler}s created by this
     * endpoint.
     */
    private final Set<SocketHandler> handlers = new HashSet<SocketHandler>();

    /**
     * The socket which endpoint listens for connections.
     */
    private ServerSocket serverSocket = null;

    /**
     * 
     * Setups a {@link SocketEndpoint},
     * 
     * @param port
     * @param eventQueue
     */
    public SocketEndpoint(final int port, final BlockingQueue<LoggingEvent> eventQueue) {
        super(port, eventQueue);
        logger.debug("SocketEndpoint ( " + port + " )");

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eye4j.com.Endpoint#openConnection()
     */
    @Override
    protected void openConnection() throws IOException {

        try {

            // bound port.
            this.serverSocket = new ServerSocket(this.port);

            /* and start thread to listen for incoming connections. */
            final Thread listenerThread = new Thread(this, "SocketEndpoint-Thread");
            listenerThread.start();

        } catch (final IOException e) {
            logger.error("Exception throwned: " + e.getMessage());
            throw e;
        } catch (final IllegalThreadStateException e) {
            logger.error("Exception throwned: " + e.getMessage());
            e.printStackTrace();
        } catch (final SecurityException e) {
            logger.error("Exception throwned: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eye4j.com.Endpoint#closeConnection()
     */
    @Override
    protected void closeConnection() throws IOException {
        /* try to close socket */
        try {
            if (this.serverSocket != null) {
                logger.debug("socket closeConnection()");
                this.serverSocket.close();
            }
        } catch (final IOException e) {
            logger.error("Exception occurs closing serverSocket: " + e.getMessage());
            throw e;
        } catch (final Exception e) {
            logger.error("Exception throwned: " + e.getMessage());
        }
    }

    /**
     * It starts running when method {@link SocketEndpoint#openConnection()} is
     * called.
     */
    @Override
    public void run() {
        logger.debug("SocketEndpoint: run()");
        this.setState(State.LISTENING);

        /*
         * Only stop when state is set to STARTED, this is done by
         * <b>closeConnection</b> method.
         */
        while (!this.getState().equals(State.DISCONNECTED)) {

            Socket incomingConnection = null;
            try {

                // accept connection
                incomingConnection = this.serverSocket.accept();
                logger.info("incoming connection accepted: " + incomingConnection.getInetAddress().getHostAddress());

                // setup a handler for this incoming connection
                final SocketHandler handler = new SocketHandler(incomingConnection, this.eventQueue);

                /*
                 * Remember handler to be able to close this incoming
                 * connection.
                 */
                this.handlers.add(handler);

                /* Start handler thread */
                handler.setDaemon(true);
                handler.start();

            } catch (final IOException e) {
                logger.error("Expect error with message: " + e.getMessage() + " : " + this.getState());

                if (incomingConnection != null) {
                    try {
                        incomingConnection.close();
                    } catch (final IOException io) {
                        // ignore
                    }
                    incomingConnection = null;
                }
            } catch (final Exception e) {
                logger.debug("SocketEndpoint exception: " + e.getMessage());
            }
        }

        logger.debug("SocketEndpoint: disconnect handlers: " + this.handlers.size());
        for (final SocketHandler handler : this.handlers) {
            handler.disconnect();
        }
        this.handlers.clear();
    }

}
