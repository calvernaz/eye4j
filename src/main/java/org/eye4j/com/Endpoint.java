/*
 * Copyright (C) The reproduction, transmission or use of this document or its
 * contents is not permitted without express written authorization. All rights,
 * including rights created by patent grant or registration of a utility model
 * or design, are reserved. Modifications made to this document are restricted
 * to authorized personnel only. Technical specifications and features are
 * binding only when specifically and expressly agreed upon in a written
 * contract.
 */
package org.eye4j.com;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.spi.LoggingEvent;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * This class represents an endpoint, which wraps a {@link Receiver}, so that
 * connections can establish a connection using a protocol.
 * </p>
 * <p>
 * This class must be extended by endpoints that suppport a certain protocol as
 * e.g. <code>zeroconf</code>.
 * </p>
 * <p>
 * This is the abstract class that has to be implemented by all Endpoints. An
 * Endpoint enables other peers to connect to a {Receiver receiver} with help of
 * a given protocol.
 * </p>
 * <p>
 * An Endpoint can be in four states:
 * <ul>
 * <li><code>STARTED</code>
 * <li><code>LISTENING</code></li>
 * <li><code>ACCEPT</code></li>
 * <li><code>DISCONNECT</code></li>
 * </ul>
 * </p>
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">name</a>
 *         <p>
 *         In state <code>STARTED</code> the endpoint has been initialised but
 *         does not listen to (possibly) incoming messages from network. An
 *         endpoint gets into this state if it is created with help of its
 *         constructor. <br/>
 *         <br/>
 *         In state <code>LISTENING</code> the endpoint accepts messages that
 *         are received from the network to update this endpoint. The transition
 *         to this state is made by invocation of {@link #listen()}. <br/>
 *         <br/>
 *         In state <code>ACCEPT</code>. This endpoint accepts events from
 *         network, that request storage. The transition to this state is made
 *         by invocation of {@link #accept()}.
 *         </p>
 */
public abstract class Endpoint {

    /**
     * The Enum State.
     */
    public enum State {

        /** The STARTED. */
        STARTED,
        /** The LISTENING. */
        LISTENING,
        /** The ACCEPT. */
        ACCEPT,
        /** The DISCONNECTED. */
        DISCONNECTED
    }

    /** The state. */
    private State state;

    /** Socket port number. */
    protected int port;

    protected final BlockingQueue<LoggingEvent> eventQueue;

    /**
     * Instantiates a new endpoint. Set the internal state as <b>STARTED</b>.
     * 
     * @param port
     *            the port
     */
    protected Endpoint(final int port, final BlockingQueue<LoggingEvent> eventQueue) {
        this.state = State.STARTED;
        this.port = port;
        this.eventQueue = eventQueue;
    }

    /**
     * Tell this endpoint that it can listen incoming events from network;.
     * 
     * @throws IOException
     */
    public final void listen() throws IOException {
        this.openConnection();
        this.state = State.LISTENING;
    }

    /**
     * Tell this endpoint to disconnect and close all connections. If this
     * method has been invoked the endpoint must be not reused!!!
     * 
     * @throws IOException
     */
    public final void disconnect() throws IOException {
        this.state = State.DISCONNECTED;
        this.closeConnection();
    }

    /**
     * Gets the state.
     * 
     * @return Returns the state.
     */
    public final State getState() {
        return this.state;
    }

    /**
     * Sets the state.
     * 
     * @param state1
     *            the new state
     */
    protected final void setState(final State state1) {
        this.state = state1;
    }

    /**
     * To be implemented by sub classes. This method is called by
     * {@link #listen()} to make it possible for other endpoints (events log
     * senders) to connect to this point
     * 
     * @throws IOException
     */
    protected abstract void openConnection() throws IOException;

    /**
     * This method has to be overwritten by sub classes and is invoked by
     * {@link #disconnect()}to close connection.
     * 
     * @throws IOException
     */
    protected abstract void closeConnection() throws IOException;

}
