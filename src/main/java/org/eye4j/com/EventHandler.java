/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.com;

import java.util.Observable;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.eye4j.core.Viewer;
import org.eye4j.database.IDao;



/**
 * EventHandler is responsible for reading events from queue and notify
 * observers. The {@link Viewer} class must be added to observers list using
 * {@link EventHandler#addObserver(java.util.Observer)}.
 * 
 * @author Cesar Alvernaz, Feb 14, 2010
 */
final public class EventHandler extends Observable implements Runnable {
  
  /** The Constant logger. */
  private static final Logger logger = Logger.getLogger(EventHandler.class);
  
  /** The event queue. */
  private final BlockingQueue<LoggingEvent> eventQueue;
  
  /** The database. */
  private final IDao database;
  
  
  
  /**
   * Instantiates a new event handler.
   * 
   * @param eventQueue
   * the event queue is a shared resource between
   * @param dao
   * the dao {@link EventHandler} and {@link SocketHandler}.
   */
  public EventHandler(final BlockingQueue<LoggingEvent> eventQueue, final IDao dao) {
    this.eventQueue = eventQueue;
    this.database = dao;
  }
  
  
  
  /**
   * Insert log event into database.
   * 
   * @param event
   * the event
   * @return true, if successful
   */
  private boolean insertLogEventIntoDatabase(final LoggingEvent event) {
    return database.insertLogEventIntoDatabase(event);
  }
  
  
  
  /**
   * Run.
   */
  @Override
  public void run() {
    
    try {
      logger.debug("EventHandler: run()");
      while ( !Thread.currentThread().isInterrupted()) {
        
        /* add to specific observer list and notify. */
        final LoggingEvent event = this.eventQueue.take();
        setChanged();
        notifyObservers(event);
        
        /* This method must go out from here. It must commit only valid logs ? */
        insertLogEventIntoDatabase(event);
      }
    }
    catch (final InterruptedException e) {
      logger.debug("EventHandler interrupted ...");
      Thread.currentThread().interrupt();
    }
  }
  
  
  
  /**
   * Cancel.
   */
  public void cancel() {
    Thread.currentThread().interrupt();
  }
  
}
