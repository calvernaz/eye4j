/*
 * Copyright (C) The reproduction, transmission or use of this document or its
 * contents is not permitted without express written authorization. All rights,
 * including rights created by patent grant or registration of a utility model
 * or design, are reserved. Modifications made to this document are restricted
 * to authorized personnel only. Technical specifications and features are
 * binding only when specifically and expressly agreed upon in a written
 * contract.
 */
package org.eye4j.com;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

/**
 * This class handles {@link LoggingEvent events} for a single incoming
 * connection from another peer. This class is initializated by {@see
 * SocketEndpoint}.
 * 
 * @author Cesar Alvernaz, Jan 10, 2010
 */
final class SocketHandler extends Thread {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(SocketHandler.class);

    /** The socket over that this {@link SocketHandler} receives requests. */
    private Socket client;

    /**
     * Indicates if this RequestHandler is connected. Used in {@link #run()} to
     * determine if this is still listening for requests.
     */
    boolean connected = true;

    /** {@link ObjectInputStream} to read {@link LoggingEvent event}from. */
    private ObjectInputStream inputStream;

    /** This is the queue where events will be consumed. */
    private final BlockingQueue<LoggingEvent> eventQueue;

    /**
     * Creates a new instance of {@link SocketHandler}.
     * 
     * @param client
     *            the client, the socket to received data from remote log4j
     *            instances.
     * @param eventQueue
     *            the event queue, that will keep {@link LoggingEvent} events
     *            for further consume.
     * @throws IOException
     *             Thrown if the establishment of a connection over the provided
     *             socket fails.
     */
    public SocketHandler(final Socket client, final BlockingQueue<LoggingEvent> eventQueue) throws IOException {

        this.client = client;
        this.eventQueue = eventQueue;

        try {
            this.inputStream = new ObjectInputStream(this.client.getInputStream());
        } catch (final IOException e) {
            throw e;
        }
    }

    /**
     * Disconnect this {@link SocketHandler}. Forces the socket, which this
     * {@link SocketHandler} is bound to, to be closed.
     */
    public void disconnect() {

        if (this.connected) {

            this.connected = false;
            try {
                this.inputStream.close();
                this.inputStream = null;
            } catch (final IOException e) {
                logger.debug("Exception while closing input stream: " + this.inputStream);
            }

            try {
                this.client.close();
                this.client = null;

                eventQueue.clear();
            } catch (final IOException e) {
                logger.debug("Exception while closing socket: " + this.client);
            }
        }

        logger.debug("Client handler disconnect.");

    }

    /**
     * Listens for incoming requests send over the {@link #client} of this
     * thread.
     */
    @Override
    public void run() {

        while (this.connected) {
            LoggingEvent event;

            /* wait for incoming requests */
            try {

                // read event from socket
                event = (LoggingEvent) this.inputStream.readObject();

                // put event in queue for consumers.
                this.eventQueue.put(event);

            } catch (final EOFException e) {
                logger.debug("Reached EOF, closing connection");
                disconnect();
            } catch (final IOException e) {
                logger.debug(e.getMessage());
                disconnect();
            } catch (final ClassNotFoundException e) {
                logger.debug("Class of a serialized object cannot be found: " + e.getMessage());
                disconnect();

            } catch (final InterruptedException e) {
                logger.debug("InterruptedException: " + e.getMessage());
                disconnect();
            }
        }
    }

}
