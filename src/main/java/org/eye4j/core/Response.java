/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Class Response.
 * 
 * 
 */
public class Response {

    /** The error. */
    private String error;

    /** The error message. */
    private String errorMessage;

    /**
     * Gets the error.
     * 
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Sets the error.
     * 
     * @param error
     *            the error to set
     */
    public void setError(final String error) {
        this.error = error;
    }

    /**
     * Gets the error message.
     * 
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the error message.
     * 
     * @param errorMessage
     *            the errorMessage to set
     */
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Returns a string in JSON enconding.
     */
    @Override
    public String toString() {
        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        final String jsonString = gson.toJson(this);
        return jsonString;
    }

}
