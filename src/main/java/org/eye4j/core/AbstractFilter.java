/**
 * 
 */
package org.eye4j.core;

import java.util.Iterator;
import java.util.NoSuchElementException;



/**
 * @author calvernaz
 */
public abstract class AbstractFilter<T> {
  
  private boolean active = true;
  
  
  
  public abstract boolean passes(T object);
  
  
  
  protected void setActive() {
    this.active = true;
  }
  
  
  
  protected void setInactive() {
    this.active = false;
  }
  
  
  
  protected boolean isActive() {
    return active;
  }
  
  
  
  public abstract void dropFilter(String filter);
  
  
  
  public Iterator<T> filter(Iterator<T> iterator) {
    return new FilterIterator(iterator);
  }
  
  
  
  public Iterable<T> filter(final Iterable<T> iterable) {
    return new Iterable<T>() {
      
      @Override
      public Iterator<T> iterator() {
        return filter(iterable.iterator());
      }
    };
  }
  
  /**
   * Private filter iterator class. Enables the "foreach" style iteration.
   * 
   * @author calvernaz
   */
  private class FilterIterator implements Iterator<T> {
    
    private final Iterator<T> iterator;
    
    private T next;
    
    
    
    private FilterIterator(Iterator<T> iterator) {
      this.iterator = iterator;
      toNext();
    }
    
    
    
    @Override
    public boolean hasNext() {
      return next != null;
    }
    
    
    
    @Override
    public T next() {
      if (next == null)
        throw new NoSuchElementException();
      T returnValue = next;
      toNext();
      return returnValue;
    }
    
    
    
    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
    
    
    
    private void toNext() {
      next = null;
      while (iterator.hasNext()) {
        T item = iterator.next();
        if (item != null && passes(item)) {
          next = item;
          break;
        }
      }
    }
  } // end if inner class
  
}