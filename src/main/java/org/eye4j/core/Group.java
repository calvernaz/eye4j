package org.eye4j.core;

import java.util.List;



public class Group {
  
  private String groupName;
  
  private List<String> packageNames;
  
  
  
  public Group(String groupName, List<String> packageNames) {
    this.groupName = groupName;
    this.packageNames = packageNames;
  }
  
  
  
  public String getGroupName() {
    return groupName;
  }
  
  
  
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }
  
  
  
  public List<String> getPackageNames() {
    return packageNames;
  }
  
  
  
  public void setPackageNames(List<String> packageNames) {
    this.packageNames = packageNames;
  }
}
