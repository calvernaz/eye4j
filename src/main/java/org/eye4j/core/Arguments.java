/**
 * 
 */
package org.eye4j.core;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * This class represents the argument in request list.
 * 
 * @author Cesar Alvernaz
 * 
 */
public class Arguments implements Serializable {

    /** The key. */
    private String key;

    /** The value. */
    private Object value;

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Arguments() {
        //
    }

    /**
     * @return the key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * @param key
     *            the key to set
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        final Gson toString = new GsonBuilder().setPrettyPrinting().create();
        return toString.toJson(this);
    }

}
