/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.core;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Class Service.
 */
public final class Service {

    /** The default port for listen connections. */
    public static final int DEFAULT_PORT = 4445;

    private final transient Gson gson;

    /* The category name */
    private final String category;

    /* The service name, friendly name :) */
    private final String name;

    /* Unique identifier, random one! */
    private final String id;

    private final int port;

    public Service(final String category, final String name, final int port) {

        this.category = category;

        this.name = name;

        id = toHexString(this.name);

        this.port = port;

        gson = new GsonBuilder().setPrettyPrinting().create();

    }

    public Service(final String name) {
        this("Unknown", name, DEFAULT_PORT);
    }

    /**
     * Returns a string of the hexadecimal representation of the first
     * <code>n</code> bytes of this ID, including leading zeros.
     * 
     * @param name
     *            the name
     * @return Hex string of ID
     */
    public static final String toHexString(final String name) {

        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        messageDigest.reset();
        messageDigest.update(name.getBytes());

        final byte[] md5sum = messageDigest.digest();
        final BigInteger bigInt = new BigInteger(1, md5sum);

        return bigInt.toString(16);
    }

    public String toJSONformat() {
        return gson.toJson(this);
    }
}
