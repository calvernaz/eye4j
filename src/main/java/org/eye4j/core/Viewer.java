/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.core;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.eye4j.filter.FilterManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



/**
 * The Class Viewer.
 * View implements the Observer pattern. Has a relation one-to-one with a
 * "logpad" in the browser.
 */
public class Viewer implements Observer {
  
  /** The Constant logger. */
  private static final Logger logger = Logger.getLogger(Viewer.class);
  
  /** The gson. */
  private final Gson gson;
  
  /** The out stream. */
  private final OutputStreamWriter outStream;
  
  
  
  /**
   * Instantiates a new viewer.
   * 
   * @param stream
   * the {@link OutputStream} stream. The log events are sent from
   * this stream.
   * @throws IOException
   */
  public Viewer(final OutputStream stream) throws IOException {
    super();
    this.outStream = new OutputStreamWriter(stream, "UTF8");
    this.gson = new GsonBuilder().create();
  }
  
  
  
  /*
   * (non-Javadoc)
   * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
   */
  @Override
  public void update(final Observable o, final Object logEvent) {
    
    try {
      
      if (FilterManager.COMPOUND.passes((LoggingEvent)logEvent)) {
        
        logger.debug(((LoggingEvent)logEvent).getLevel().toString() + " " + gson.toJson(logEvent));
        
        // JSONized event and send to client
        write(logEvent);
        
      }
      
    }
    catch (final IOException e) {
      logger.debug("Delete observer ...");
      e.printStackTrace();
      o.deleteObserver(this);
    }
  }
  
  
  
  private void write(final Object logEvent) throws IOException {
    this.outStream.write(this.gson.toJson(logEvent) + "\r\n");
    // flush it
    this.outStream.flush();
  }
  
}
