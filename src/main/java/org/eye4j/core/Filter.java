package org.eye4j.core;

import org.apache.log4j.Logger;



/**
 * This is the super <i>abstract</i> class for filter settings.
 * 
 * @author Cesar Alvernaz
 */
public abstract class Filter {
  
  /** The Constant logger. */
  private static final Logger logger = Logger.getLogger(Filter.class);
  
}
