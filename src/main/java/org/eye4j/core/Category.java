/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.core;

import java.util.List;

/**
 * Include here the purpose of the class.
 * categories: [
 * { id: CATEGORYID,
 * name: CATEGORYNAME,
 * services: [
 * { id: SERVICEID,
 * name : SERVICENAME
 * }, ...
 * ]
 * 
 * @author <a href="mailto:cesar.alvernaz@gmail.com">name</a>
 * @see Include references to other classes or remove this line
 */
public class Category {
    
    /** The id. */
    private int id;
    
    /** The name. */
    private String name;
    
    private List<Service> services;
    
    /**
     * Instantiates a new categorie.
     */
    public Category(final int id, final String name) {
        this.id = id;
        this.name = name;
    }
    
    /**
     * @return Returns the id.
     */
    public int getId() {
        return id;
    }
    
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return Returns the services.
     */
    public List<Service> getServices() {
        return services;
    }
    
    /**
     * @param id
     *            The id to set.
     */
    public void setId(final int id) {
        this.id = id;
    }
    
    /**
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }
    
    /**
     * @param services
     *            The services to set.
     */
    public void setServices(final List<Service> services) {
        this.services = services;
    }
    
}
