/*
 * Copyright (C)
 * The reproduction, transmission or use of this document or its contents
 * is not permitted without express written authorization.
 * All rights, including rights created by patent grant or
 * registration of a utility model or design, are reserved.
 * Modifications made to this document are restricted to authorized personnel
 * only.
 * Technical specifications and features are binding only when specifically
 * and expressly agreed upon in a written contract.
 */
package org.eye4j.core;

import java.io.Serializable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * This class represents a request. The request messages are sent for
 * configuration purposes, are enconded in JSON format and the deserialized to
 * this class.
 * 
 * @see <a
 *      href="http://redmine.dgeez.com/wiki/eye4j/JSON_Protocol#21-Requests">JSON
 *      Request Protocol</a>
 * @author <a href="mailto:cesar.alvernaz@gmail.com">Cesar Alvernaz</a>
 */
public class Request implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The method. */
    private String method;

    /** The arguments. */
    private List<Arguments> arguments;

    /** The tag. */
    private String tag;

    /**
     * Gets the method.
     * 
     * @return Returns the method.
     */
    public String getMethod() {
        return this.method;
    }

    /**
     * Gets the tag.
     * 
     * @return the tag
     */
    public String getTag() {
        return this.tag;
    }

    /**
     * Sets the method.
     * 
     * @param method
     *            The method to set.
     */
    public void setMethod(final String method) {
        this.method = method;
    }

    /**
     * Sets the tag.
     * 
     * @param tag
     *            The tag to set.
     */
    public void setTag(final String tag) {
        this.tag = tag;
    }

    /**
     * @return
     */
    public List<Arguments> getArguments() {
        return this.arguments;
    }

    /**
     * @param arguments
     */
    public void setArguments(final List<Arguments> arguments) {
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        final Gson toString = new GsonBuilder().setPrettyPrinting().create();
        return toString.toJson(this);
    }

}
