/**
 * 
 */
package org.eye4j.database;

import java.util.List;

import org.apache.log4j.spi.LoggingEvent;



/**
 * The Interface IDao.
 * 
 * @author kron
 */
public interface IDao {
  
  /**
   * Insert log event.
   * 
   * @param event
   * the event
   * @return true, if successful
   */
  public boolean insertLogEventIntoDatabase(LoggingEvent event);
  
  
  
  /**
   * Find log event.
   * 
   * @param event
   * the event
   * @return true, if successful
   */
  public List<LoggingEvent> findLogEventInDatabase(final LoggingEvent event);
  
}
