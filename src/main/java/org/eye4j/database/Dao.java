/**
 * 
 */
package org.eye4j.database;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.spi.LoggingEvent;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;



/**
 * @author kron
 */
public class Dao extends AbstractDao {
  
  private final LoggingEventBsonifier bsonifier;
  
  
  
  /**
   * Instantiates a new dao.
   */
  public Dao() {
    
    this.bsonifier = new LoggingEventBsonifierImpl();
  }
  
  
  
  /*
   * (non-Javadoc)
   * @see
   * org.eye4j.database.IDao#insertLogEventIntoDatabase(org.apache.log4j.spi
   * .LoggingEvent)
   */
  @Override
  public boolean insertLogEventIntoDatabase(final LoggingEvent event) {
    
    return insert(this.bsonifier.bsonify(event));
    
  }
  
  
  
  /*
   * (non-Javadoc)
   * @see
   * org.eye4j.database.IDao#findLogEventIntoDatabase(org.apache.log4j.spi
   * .LoggingEvent)
   */
  public List<LoggingEvent> findLogEventInDatabase(final LoggingEvent event) {
    
    List<LoggingEvent> events = new ArrayList<LoggingEvent>();
    BasicDBObject logEvent = null;
    LoggingEvent loggingEvent = null;
    
    DBCursor cursor = find(this.bsonifier.bsonify(event));
    while (cursor.hasNext()) {
      
      logEvent = (BasicDBObject)cursor.next();
      
      loggingEvent.setProperty("timeStamp", logEvent.get("timestamp").toString());
      loggingEvent.setProperty("level", logEvent.get("level").toString());
      loggingEvent.setProperty("thread", logEvent.get("thread").toString());
      loggingEvent.setProperty("message", logEvent.get("message").toString());
      loggingEvent.setProperty("loggerName", logEvent.get("loggerName").toString());
      
      events.add(loggingEvent);
    }
    
    return events;
  }
  
}
