/**
 * 
 */
package org.eye4j.database;

import org.apache.log4j.spi.LoggingEvent;

import com.mongodb.DBObject;

/**
 * Interface implemented by classes that create a BSON representation of a Log4J
 * LoggingEvent. LoggingEventBsonifierImpl is the default implementation.
 */
public interface LoggingEventBsonifier {

    /**
     * BSONifies a single Log4J LoggingEvent object.
     * 
     * @param loggingEvent
     *            The LoggingEvent object to BSONify <i>(may be null)</i>.
     * @return The BSONified equivalent of the LoggingEvent object <i>(may be
     *         null)</i>.
     */
    DBObject bsonify(LoggingEvent loggingEvent);
}
