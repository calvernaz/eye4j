/**
 * 
 */
package org.eye4j.database;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;



// TODO: Auto-generated Javadoc
/**
 * The Class AbstractDaoProxy.
 * 
 * @author kron
 */
public abstract class AbstractDao implements IDao {
  
  /** The Constant logger. */
  private static final Logger logger = Logger.getLogger(AbstractDao.class);
  
  /** The Constant DEFAULT_MONGO_DB_DATABASE_NAME. */
  private static final String DEFAULT_MONGO_DB_DATABASE_NAME = "eye4j-database";
  
  private static final String DEFAULT_MONGO_DB_COLLECTION_NAME = "logevents";
  
  /** The database connection. */
  private Mongo databaseConnection = null;
  
  /** The database. */
  private DB database = null;
  
  /** The collection. */
  private DBCollection collection = null;
  
  
  
  /**
   * Setups a database connection.
   */
  public AbstractDao() {
    
    try {
      this.databaseConnection = new Mongo();
    }
    catch (final UnknownHostException e) {
      e.printStackTrace();
    }
    catch (final MongoException e) {
      e.printStackTrace();
    }
    
    this.database = this.databaseConnection.getDB(DEFAULT_MONGO_DB_DATABASE_NAME);
    
    setCollection(this.database.getCollection(DEFAULT_MONGO_DB_COLLECTION_NAME));
    
    logger.info("Mongo Database: " + this.database.toString());
    
  }
  
  
  
  /**
   * @see org.apache.log4j.Appender#close()
   */
  public void close() {
    if (this.databaseConnection != null) {
      this.collection = null;
      this.databaseConnection.close();
    }
  }
  
  
  
  /**
   * Insert.
   * 
   * @return true, if successful
   */
  protected boolean insert(final DBObject bson) {
    
    try {
      getCollection().insert(bson);
    }
    catch (final MongoException e) {
      logger.error("Exception throwned: " + e.getMessage());
      return false;
    }
    return true;
  }
  
  
  
  /**
   * Find collection that satisfies condition
   * 
   * @param findObject
   * @return
   */
  protected DBCursor find(final DBObject condition) {
    return getCollection().find(condition);
  }
  
  
  
  /**
   * Sets the collection.
   * 
   * @param collection
   * the new collection
   */
  private void setCollection(final DBCollection collection) {
    this.collection = collection;
  }
  
  
  
  /**
   * @return The MongoDB collection to which events are logged.
   */
  protected DBCollection getCollection() {
    return this.collection;
  }
}
